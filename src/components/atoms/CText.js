import React, {Component} from 'react';
import {Text, View} from 'react-native';

export default class CText extends Component {
  render() {
    return (
      <View>
        <Text {...this.props} >
          {this.props.children}
        </Text>
      </View>
    );
  }
}