import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {Splash, Login, Crud, Lifecycle, Redux, Array, Reusable} from '../screen';

const Stack = createNativeStackNavigator();

const Route = () => {
  const Off = {headerShown: false};
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen name="Splash" component={Splash} options={Off} />
        <Stack.Screen name="Crud" component={Crud} options={Off} />
        <Stack.Screen name="Login" component={Login} options={Off} />
        <Stack.Screen name="Lifecycle" component={Lifecycle} options={Off} />
        <Stack.Screen name="Redux" component={Redux} options={Off} />
        
        <Stack.Screen name="Reusable" component={Reusable} options={Off} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Route;
