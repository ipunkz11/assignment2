const initialState = {
  test: 'Nama Pelajar',
  students: [
    {id: 1, name: 'Desa', address: 'California'},
    {id: 2, name: 'Desi', address: 'Berlin'},
    {id: 3, name: 'Desu', address: 'Tokyo'},
    {id: 4, name: 'Duda', address: 'Quebec'},
    {id: 5, name: 'Dadu', address: 'London'},
  ],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD-STUDENT':
      return {
        ...state,
        students: [...state.students, action.payload],
      };
    case 'DELETE-STUDENT':
      return {
        ...state,
        students: state.students.filter(value => {
          return value.id != action.payload;
        }),
      };
    default:
      return state;
  }
};

export default reducer;
