import { Text, View } from 'react-native';
import React, { Component } from 'react';

export default class index extends Component {
    constructor(){
        super()
        this.state={
            x:[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]
        }
    }
  render() {
      const {x} = this.state
    return (
      <View style={{flex:1, justifyContent:'center', alignItems:'center', flexDirection:'row'}}>
      <View>
        {
          x.length > 0 && x.map((v,i) => {
            return(
              <View key={i}>{v % 2 === 0 && <Text>{v}</Text> }</View>
            )
          })
        }
      </View>
      <View>
        {
          x.length > 0 && x.map((v,i) => {
            return(                         
              <View key={i}>{i % 2 != 0 && <Text>{i}</Text> }</View>
            )
          })
        }
      </View>
      </View>
      );
  }
}
