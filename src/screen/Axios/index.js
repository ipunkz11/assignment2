import React, {Component} from 'react';
import axios from 'axios';
import {Text, View, ScrollView} from 'react-native';

export default class Axios extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    axios
      .get('https://dummyjson.com/users')
      .then(result => this.setState({data: result.data.users}));
  }

  render() {
    const {data} = this.state;
    // console.log(data);
    return (
      <View>
        {data.length > 0 &&
          data.map((value, index) => {
            return (
              <ScrollView key={index}>
                <Text
                  style={{
                    paddingVertical: 10,
                    textAlign: 'center',
                    fontWeight: 'bold',
                  }}>
                  {value.firstName} | {value.lastName}| {value.maidenName}
                </Text>
              </ScrollView>
            );
          })}
      </View>
    );
  }
}
