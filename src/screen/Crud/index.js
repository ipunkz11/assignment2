import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  TextInput,
} from 'react-native';

export default class Crud extends Component {
  constructor() {
    super();
    this.state = {
      data: [
        {id: 1, name: 'Jaka', address: 'Pandeglang'},
        {id: 2, name: 'Jaki', address: 'Serang'},
        {id: 3, name: 'Jika', address: 'Tangerang'},
        {id: 4, name: 'Nani', address: 'Palembang'},
        {id: 5, name: 'Nina', address: 'Sumedang'},
      ],
      name: 'Saya',
      address: 'Lupalah',
    };
  }

  _createData() {
    const {name, address} = this.state;
    this.setState({name: name, address: address});
  }

  _delete(nilai) {
    const {data, name, address} = this.state;
    const dataBaru = data.filter(x => {
      x.id != nilai;
    });
    this.setState({
      data: dataBaru,
    });
  }
  render() {
    const {data, name, address} = this.state;
    return (
      <View style={{backgroundColor: 'pink', flex: 1}}>
        <Text style={styles.title}>CRUD</Text>

        <View style={{alignItems: 'center'}}>
          <View style={styles.data}>
            <View style={styles.wrap}>
              <Text style={{fontWeight: 'bold'}}>Name</Text>
              <Text style={{fontWeight: 'bold'}}>Address</Text>
              <Text style={{fontWeight: 'bold'}}></Text>
            </View>
            <View style={styles.wrap}>
              <Text>{name}</Text>
              <Text>{address}</Text>
              <TouchableOpacity onPress={() => this._createData()}>
                <Text
                  style={{
                    textAlign: 'right',
                    color: 'red',
                    fontWeight: 'bold',
                  }}>
                  X
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <TextInput
            onChangeText={input => this.setState({name: input})}
            style={styles.input}
          />
          <TextInput
            onChangeText={input => this.setState({address: input})}
            style={styles.input}
          />
          <TouchableOpacity
            style={styles.button}
            onPress={() => this._createData()}>
            <Text style={{textAlign: 'center', fontWeight: 'bold'}}>
              ADD DATA
            </Text>
          </TouchableOpacity>
        </View>
        {/* <Text
          style={{
            marginVertical: 10,
            fontWeight: 'bold',
          }}>
          CRUD in State
        </Text>
        <View style={styles.table}>
          {data.length > 0 &&
            data.map((value, i) => {
              return (
                <View style={{flexDirection: 'row'}} key={i}>
                  <Text
                    style={{
                      textAlign: 'center',
                      fontWeight: 'bold',
                    }}>
                    {value.id}
                    {value.name}
                    {value.address}
                    <TouchableOpacity onPress={() => this._delete(value.id)}>
                      <Text style={{color: 'red', paddingLeft: 10}}> X</Text>
                    </TouchableOpacity>
                  </Text>
                </View>
              );
            })}
        </View>
         */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  table: {
    flexDirection: 'column',
    borderWidth: 1,
    padding: 10,
    width: '80%',
    borderRadius: 10,
    alignItems: 'center',
  },
  title: {
    textAlign: 'center',
    marginTop: 20,
    fontSize: 24,
    fontWeight: 'bold',
    color: 'red',
  },
  input: {
    width: '80%',
    borderWidth: 2,
    borderRadius: 10,
    marginVertical: 5,
    borderColor: 'aqua',
    padding: 10,
  },
  button: {
    backgroundColor: 'aqua',
    width: '80%',
    padding: 10,
    borderRadius: 10,
  },
  data: {
    marginVertical: 10,
    paddingVertical: 10,
    borderBottomWidth: 1,
    width: '80%',
  },
  wrap: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
});
