import React, {Component} from 'react';
import {TouchableOpacity, Text, View, StyleSheet} from 'react-native';

export default class index extends Component {
  constructor() {
    super();
    this.state = {
      kopi: 'Warna Hitam',
    };
    console.log('This is a Constructor');
  }

  componentDidMount() {
    console.log('This is a ComponentDidMount');
  }

  componentDidUpdate() {
    console.log('This is a ComponentDidUpdate');
  }

  componentWillUnmount() {
    console.log('This is a ComponentWillUnmount');
  }

  _updateKopi = () => {
    const {kopi} = this.state;
    this.setState({
      kopi: 'Hitam Banget',
    });
  };

  render() {
    console.log('This is a Render');
    const {kopi} = this.state;
    return (
      <View style={styles.body}>
        <Text style={{color: 'red', fontWeight: 'bold'}}>{kopi}</Text>
        <View style={{height: 10}}></View>
        <TouchableOpacity
          style={styles.tombol}
          onPress={() => this._updateKopi()}>
          <Text style={{textAlign: 'center', fontWeight: 'bold'}}>Change</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  body: {
    flex: 1,
    backgroundColor: 'green',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tombol: {
    backgroundColor: 'aqua',
    width: '80%',
    borderRadius: 10,
    padding: 10,
  },
});
