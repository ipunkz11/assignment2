import React, { Component } from 'react';
import {
  ImageBackground,
  Text,
  Image,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  Alert,
} from 'react-native';
import { Logo, Pattern } from '../../assets';
import Google from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/Entypo';
import User from 'react-native-vector-icons/EvilIcons';
import axios from 'axios';
import CButton from '../../components/atoms/CButton';

export default class Login extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
      username: '',
      password: '',
    };
  }

  componentDidMount() {
    axios
      .get('https://dummyjson.com/users')
      .then(result =>
        this.setState({
          data: result.data.users,
        }),
      )
      .catch(err => {
        console.error(err);
      });
  }

  _compare({ data, username, password }) {
    const getFilter = data.filter(value => {
      return value.username == username && value.password == password;
    });
    // return getFilter
    console.log(getFilter)
    if (getFilter.length > 0) {
      this.props.navigation.navigate('Crud')
    } else {
      Alert.alert('Login Failed')
    }
  }


  _navigate({ navigation }) {
    navigation.navigate('Crud')
  }


  render() {
    const { data } = this.state;
    return (
      <View style={styles.body}>
        <ImageBackground style={styles.bg} source={Pattern}>
          <View style={styles.wrap}>
            <Image source={Logo} style={styles.logo} />
            <Text style={styles.title}>Pasihan Tuang</Text>
            <Text style={styles.paragraph}>Delivery Favourite Food</Text>
          </View>
        </ImageBackground>
        <View style={styles.wrap}>
          <TextInput
            onChangeText={value => this.setState({ username: value })}
            style={styles.input}
            placeholder="Username"
            placeholderTextColor={'green'}
          />
          <User name="user" size={35} style={styles.iconMan} />
          <TextInput
            onChangeText={value => this.setState({ password: value })}
            style={styles.input}
            placeholder="Password"
            placeholderTextColor={'green'}
            secureTextEntry={true}
          />
          <Icon name="lock" size={30} style={styles.iconLock} />
          <Text style={styles.text1}>Or Continue With</Text>
          <View style={styles.flex}>
            <TouchableOpacity style={styles.tombol}>
              <Icon name="facebook" size={40} style={{ color: 'green' }} />
            </TouchableOpacity>
            <TouchableOpacity style={styles.tombol}>
              <Google name="google" size={40} style={{ color: 'green' }} />
            </TouchableOpacity>
          </View>
          <Text style={styles.text2}>Forgot Your Password?</Text>
          {/* <TouchableOpacity
            onPress={() => this._compare(this.state) && this._navigate(this.props)}
            style={styles.btnLogin}>
            <Text style={styles.text1}>LOGIN</Text>
          </TouchableOpacity> */}
          <CButton saya='Login' onPress={() => this._compare(this.state) && this._navigate(this.props)}
            style={styles.btnLogin} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  body: {
    backgroundColor: 'black',
    flex: 1,
  },
  bg: {
    flex: 1,
  },
  wrap: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  logo: {
    height: 150,
    width: 150,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 40,
    color: '#4cd137',
  },
  paragraph: {
    color: '#f5f6fa',
    fontWeight: 'bold',
    fontSize: 15,
  },
  input: {
    backgroundColor: '#2f3640',
    marginVertical: 5,
    width: '80%',
    borderRadius: 15,
    borderWidth: 3,
    borderColor: 'green',
    paddingVertical: 10,
    paddingLeft: 50,
    color: '#dcdde1',
    fontWeight: 'bold',
  },
  iconMan: {
    color: 'green',
    position: 'absolute',
    left: 55,
    top: 80,
  },
  iconLock: {
    color: 'green',
    position: 'absolute',
    left: 55,
    top: 140,
  },
  text1: {
    color: '#f5f6fa',
    fontWeight: 'bold',
    fontSize: 15,
    paddingVertical: 10,
  },
  text2: {
    color: 'green',
    fontWeight: 'bold',
    fontSize: 15,
    fontStyle: 'italic',
    marginVertical: 5,
  },
  flex: {
    flexDirection: 'row',
  },
  tombol: {
    marginHorizontal: 25,
  },
  btnLogin: {
    backgroundColor: 'green',
    padding:20,
    width: '50%',
    alignItems: 'center',
    borderRadius: 15,
  },
});
