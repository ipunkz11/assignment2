import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  Button,
  ScrollView,
} from 'react-native';
import {connect} from 'react-redux';

export class Redux extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
      name: '',
      address: '',
      id: '',
      onselect: false,
    };
  }

  componentDidMount() {}
  _addData = () => {
    const {name, address, id} = this.state;
    const data = {name, address, id};
    this.props.add(data);
    this.setState({
      name: '',
      address: '',
      id: '',
    });
  };

  _deleteData = id => {
    this.props.delete(id);
  };

  render() {
    const {students} = this.props;
    return (
      <ScrollView>
        <View style={{alignItems: 'center'}}>
          <Text style={{fontWeight: 'bold', fontSize: 20}}>
            CRUD With Redux
          </Text>
          <View style={styles.wrap}>
            {students &&
              students.map((value, index) => {
                return (
                  <View style={styles.data} key={index}>
                    <Text style={styles.id}>{value.id}</Text>
                    <Text style={styles.name}>{value.name}</Text>
                    <Text style={styles.address}>{value.address}</Text>
                    <Button
                      title="X"
                      onPress={() => this._deleteData(value.id)}
                    />
                  </View>
                );
              })}
          </View>
          <View style={styles.wrapper}>
            <TextInput
              style={styles.input}
              onChangeText={type => {
                this.setState({id: type});
              }}
            />
            <TextInput
              style={styles.input}
              onChangeText={type => {
                this.setState({name: type});
              }}
            />
            <TextInput
              style={styles.input}
              onChangeText={type => {
                this.setState({address: type});
              }}
            />
          </View>
          <View style={{height: 10}}></View>
          <Button title="Add Data" onPress={() => this._addData()} />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  wrap: {
    marginTop: 10,
    backgroundColor: 'pink',
    width: '90%',
    borderRadius: 10,
    padding: 10,
  },
  data: {
    flexDirection: 'row',
    borderWidth: 1,
    alignItems: 'center',
  },
  id: {
    width: '20%',
    textAlign: 'center',
  },
  name: {
    width: '35%',
    textAlign: 'center',
  },
  address: {
    width: '35%',
    textAlign: 'center',
  },
  wrapper: {
    borderWidth: 1,
    width: '90%',
    marginTop: 10,
    borderRadius: 10,
    backgroundColor: 'aqua',
  },
  input: {
    borderBottomWidth: 1,
    padding: 10,
  },
});

const mapStateToProps = state => {
  return {
    students: state.students,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    add: data => {
      dispatch({
        type: 'ADD-STUDENT',
        payload: data,
      });
    },
    delete: data => {
      dispatch({
        type: 'DELETE-STUDENT',
        payload: data,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Redux);
