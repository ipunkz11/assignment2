import React, {Component} from 'react';
import {Text, View} from 'react-native';
import CText from '../../components/atoms/CText.js';

export default class Reusable extends Component {
  render() {
    return (
      <View>
        <Text>
        <CText>Test</CText>
        </Text>
      </View>
    );
  }
}
