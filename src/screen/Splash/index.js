import React, {Component} from 'react';
import {Image, ImageBackground, StyleSheet, Text, View} from 'react-native';
import {Logo, Pattern} from '../../assets';

export default class Splash extends Component {
  componentDidMount() {
    const {navigation} = this.props;
    setTimeout(() => {
      navigation.replace('Login');
    }, 3000);
  }

  render() {
    return (
      <View style={styles.body}>
        <ImageBackground source={Pattern} style={styles.bg}>
          <View style={styles.wrap}>
            <Image style={styles.logo} source={Logo} />
            <Text style={styles.title}> Pasihan Tuang </Text>
            <Text style={styles.paragraph}> Deliever Favourite Food </Text>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  body: {
    backgroundColor: 'black',
    flex: 1,
  },
  bg: {
    flex: 1,
  },
  logo: {
    height: 180,
    width: 180,
  },
  wrap: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 40,
    color: '#4cd137',
  },
  paragraph: {
    color: '#f5f6fa',
    fontSize: 15,
  },
});
